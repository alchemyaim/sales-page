=== ACF Theme Code Pro ===
Contributors: aaronrutley, ben-pearson
Requires at least: 4.5.3
Tested up to: 4.7
Stable tag: 2.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

ACF Theme Code will automatically generate the code needed to implement Advanced Custom Fields in your themes!

== Changelog ==

= 2.0.0 =
* Core : Theme Code Pro now generates code based on your location rules!
* Core : Theme Code Pro now supports all official ACF Add ons!
* Core : Theme Code Pro now works when ACF Pro is included in a theme!
* Location Supported : Options Page
* Location Supported : Widget
* Location Supported : Comment
* Location Supported : Taxonomy Term
* Location Supported : User
* Location Supported : Attachment
* Add-on supported : Options Page
* Add on supported : Repeater Field
* Add on supported : Gallery Field
* Add on supported : Flexible Content Field
* Fix : Minor bug in file field example link markup
* Fix : Support for Quicklinks feature within locations

= 1.2.0 =
* Field: Clone - major improvements to the clone field code output
* New Field Supported: Address Field
* New Field Supported: Number Slider Field
* New Field Supported: Post Type Select Field
* New Field Supported: Code Field
* New Field Supported: Link Field
* New Field Supported: Link Picker Field
* New Field Supported: YouTube Picker Field
* Core: Special characters now removed from variable names
* Fix: Compatibility with CPTUI Pro Plugin

= 1.1.0 =
* Core: Quicklinks feature with anchor links to the relevant theme code block
* Core: Notice updates & various bug fixes
* Core: Plugin options screen moved under Settings

= 1.0.3 =
* Fix: Use the_sub_field method for nested File fields with return format URL

= 1.0.2 =
* Field: Fix for Post Object when using ACF 4
* Core: Various internal code improvements

= 1.0.1 =
* Field: Checkbox updated to support array
* Field: Select updated to support array

= 1.0.0 =
* First version
