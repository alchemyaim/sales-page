<?php

//Add an action that will load all widgets
add_action( 'widgets_init', 'load_widgets' );

//Function that registers the widgets
function load_widgets() {
	register_widget('social_widget');
	register_widget('rich_text_widget');
}


/*------------------
	SOCIAL WIDGET
---------------------*/


class social_widget extends WP_Widget {
	function social_widget (){
		$widget_ops = array( 'classname' => 'social', 'description' => 'A widget that displays your social links' );
		$control_ops = array( 'width' => 250, 'height' => 120, 'id_base' => 'social-widget' );
		$this->WP_Widget( 'social-widget', ' Social Widget', $widget_ops, $control_ops );
	}
	
	function widget($args, $instance){		
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		echo $before_widget;
		//echo $before_title.$title.$after_title;
			
		echo '<div class="social-links"><span class="social-title">'.$title. '</span>';
             
        if( have_rows('social_links', 'option') ): 
		while( have_rows('social_links', 'option') ): the_row(); 

		// vars
		$icon = get_sub_field('social_media_icon', 'option');
		$link = get_sub_field('social_link', 'option');
		
		if( $link ): ?>
				<a href="<?php echo $link; ?>" target="_blank" class="social-link">
		<?php endif; ?>
        		<i class="fa <?php echo $icon; ?>"></i>
		<?php if( $link ): ?>
				</a>
		<?php endif; 
			  endwhile; 
			  endif; 
			  
		echo '</div>';
			
		echo $after_widget;
	}
			
	function update($new_instance, $old_instance){
		$instance = $old_instance;	
		$instance['title'] = strip_tags($new_instance['title']);					
		return $instance;
	}		
		
	function form($instance){
		$defaults = array( 'title' => 'Social Links');
		$instance = wp_parse_args((array) $instance, $defaults);
		
?>	
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'theme'); ?></label>
				<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
			</p>
			
			<p>
				Edit social links within the <a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=theme-general-settings">Theme Options Panel</a>.
			</p>
<?php	
	
	}
}

/*--------------------
	WYSIWYG WIDGET
---------------------*/
class rich_text_widget extends WP_Widget {
	function rich_text_widget (){
		$widget_ops = array( 'classname' => 'rich_text', 'description' => 'A rich text WYSIWYG widget.' );
		$control_ops = array( 'width' => 250, 'height' => 120, 'id_base' => 'rich-text-widget' );
		$this->WP_Widget( 'rich-text-widget', 'WYSIWYG Widget', $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		echo $before_widget;
		echo $before_title.$title.$after_title;
		
		echo get_field('wysiwyg_widget', 'widget_' . $args['widget_id']);
?>

<?php
		echo $after_widget;
	}

	function update($new_instance, $old_instance){
		$instance = $old_instance;	
		$instance['title'] = strip_tags($new_instance['title']);					
		return $instance;
	}	

	function form($instance){
		$defaults = array( 'title' => 'WYSIWYG Widget');
		$instance = wp_parse_args((array) $instance, $defaults);
		
?>	
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'theme'); ?></label>
				<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
			</p>
			
<?php	
	}
}
?>