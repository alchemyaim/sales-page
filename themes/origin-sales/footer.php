        <div style="clear:both;"></div>

    </div><!--CONTAINER-->

</div><!--OUTER WRAPPER-->

<footer>
	<div id="credits" class="container">
		<div class="center">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> | <a href="<?php the_field('refund_policy_url', 'option'); ?>"><?php the_field('refund_policy_link_label', 'option'); ?></a> | <a href="<?php the_field('cancel_policy_url', 'option'); ?>"><?php the_field('cancel_policy_link_label', 'option'); ?></a> | Site Developed by <a href="http://www.alchemyandaim.com/" target="_blank">Alchemy + Aim</a></div>
	</div>
</footer>

</div>

<?php wp_footer(); ?>

<?php //Google Analytics
	if(get_field('google_analytics_location', 'option') == "footer") {
    	echo get_field('google_analytics_code', 'option');
	}
?>

<?php //Custom Javascript
	$js = get_field('custom_javascript' ,'option');
	if( !empty($js) ): ?>
    	<script>
		(function($) {
        <?php echo $js; ?>
		})(jQuery);
    	</script>
<?php endif; ?>

<a href="#" class="scrollToTop"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
</body>
</html>