<?php
/*
Template Name: Sales Page
*/
?>
<?php get_header(); ?>
<article> 

  <?php if ( have_rows( 'sales_sections' ) ): ?>
	<?php while ( have_rows( 'sales_sections' ) ) : the_row(); ?>
	
		<?php if ( get_row_layout() == 'big_video_section' ) : ?>
		<!-- VIDEO SECTION -->
		<section class="video-section">
			<div class="container">
				<div class="row">
					<div class="twelve columns">
						<div class="vid-intro-text"><?php the_sub_field( 'header_text' ); ?></div>
							<?php 
								$vid_image = get_sub_field( 'video_poster_image' );
								$vid_id = get_sub_field( 'video_id' );
							?>
			  				<div class="video-container">
    							<div class="video">
    							<img src="<?php echo $vid_image['url']; ?>">
    							<!-- <iframe width="987" height="508" src="http://www.youtube.com/embed/<?php echo $vid_id; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>-->
    							</div>
    						</div>

			<div class="center"><a href="<?php the_sub_field( 'button_url' ); ?>" class="button button-primary"><?php the_sub_field( 'button_label' ); ?></a></div>
			
					</div>
				</div>
			</div>
		</section>
			
						
		<?php elseif ( get_row_layout() == 'wysiwyg_section' ) : ?>
		<!-- WYSIWYG SECTION -->
		<section id="<?php the_sub_field( 'section_id' ); ?>" style="background-color:<?php the_sub_field( 'section_background_color' ); ?>;">
			<div class="container">
				<div class="row">
					<div class="twelve columns">
						<?php the_sub_field( 'general_content' ); ?>
					</div>
				</div>
			</div>
		</section>
	
			
	<?php elseif ( get_row_layout() == 'image_text_section' ) : ?>
	<!-- IMAGE & TEXT SECTION -->
	<?php 
		//var
		$background_image = get_sub_field( 'background_image' ); 
		$bg_text = get_sub_field( 'image_section_text' );
	?>
		<section id="<?php the_sub_field( 'section_id' ); ?>" style="background-image:url('<?php echo $background_image['url'] ?>');">
			<div class="container">
				<div class="text-outer" style="min-height:<?php echo the_sub_field( 'maximum_height' ); ?>;<?php echo the_sub_field( 'text_width_&_position' ); ?>">
					<div class="inner-text mobile-hide"><?php echo $bg_text; ?></div>
				</div>
			</div>	
		</section>
		<section class="mobile"><div class="mobile-text"><?php echo $bg_text; ?></div></section>
		
			
		<?php elseif ( get_row_layout() == 'countdown_section' ) : ?>
		<!-- COUNTDOWN SECTION -->
		<section class="count-down-section">
			<div class="container countdown-box">
				<div class="row">
					<div class="twelve columns">
						<div class="top-text"><?php the_sub_field( 'header_text' ); ?></div>
						<?php if(is_active_sidebar('count_down')) dynamic_sidebar('count_down'); ?>
						<div class="bottom-text"><?php the_sub_field( 'subheader_text' ); ?></div>
					</div>
				</div>
			</div>
			<div class="center counter-button"><a href="<?php the_sub_field( 'button_url' ); ?>" class="button button-primary"><?php the_sub_field( 'button_label' ); ?></a></div>
		</section>
		
			
		<?php elseif ( get_row_layout() == 'button' ) : ?>
		<!-- BUTTON -->
			<div id="<?php the_sub_field( 'button_id' ); ?>" class="center single-button"><a href="<?php the_sub_field( 'button_url' ); ?>" class="button button-primary"><?php the_sub_field( 'button_label' ); ?></a></div>
			
				
		<?php elseif ( get_row_layout() == 'single_testimonial' ) : ?>
		<!-- SINGLE TESTIMONIAL -->	
		<section class="single-testimonial">
			<div class="container testimonial-container">
				<div class="row">
					<div class="twelve columns">
			<?php $testimonial_image = get_sub_field( 'testimonial_image' ); ?>
			<?php if ( $testimonial_image ) { ?>
						<div class="auth-img"><img src="<?php echo $testimonial_image['url']; ?>" alt="<?php echo $testimonial_image['alt']; ?>" /></div>
			<?php } ?>
						<div class="auth-text"><?php the_sub_field( 'testimonial_text' ); ?></div>
					</div>
				</div>
			</div>
		</section>
			
			
		<?php elseif ( get_row_layout() == 'membership_blocks_section' ) : ?>
		<!-- MEMBERSHIP BLOCKS -->	
		<section class="membership-blocks">
			<div class="container testimonial-container">
				<div class="row">
					<div class="twelve columns">
					<hr />
						<h1 class="intro-text"><?php the_sub_field( 'block_intro_text' ); ?></h1>
					</div>
				</div>
				<div class="member-blocks">
					<div class="gutter-sizer"></div>
			<?php if ( have_rows( 'member_blocks' ) ) : ?>
				<?php while ( have_rows( 'member_blocks' ) ) : the_row(); ?>
				<div class="member-block">
					<div class="block-text">
					<h2 class="block-title"><?php the_sub_field( 'block_title' ); ?></h2>
					<?php the_sub_field( 'block_text' ); ?>
					</div>
					<?php $block_icon = get_sub_field( 'block_icon' ); ?>
					<?php if ( $block_icon ) { ?>
				<div class="icon"><span class="helper"></span><img src="<?php echo $block_icon['url']; ?>" alt="<?php echo $block_icon['alt']; ?>" /></div>
					<?php } ?>
				</div>
				<?php endwhile; ?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
				</div>
				<div class="row">
					<div class="twelve columns">
						<h1 class="outro-text"><?php the_sub_field( 'block_outro_text' ); ?></h1>
					</div>
				</div>
			</div>
		</section>
	<div id="member-block-graphic"></div>
			
		<?php elseif ( get_row_layout() == 'checklist_section' ) : ?>
		<!-- CHECKLIST SECTION -->	
		<section class="checklist-section">
			<div class="container">
				<div class="row">
					<div class="twelve columns">
						<h2><?php the_sub_field( 'checklist_intro_text' ); ?></h2>
					</div>
				</div>
			<?php if ( have_rows( 'checklist' ) ) : ?>
					<ul>
				<?php while ( have_rows( 'checklist' ) ) : the_row(); ?>
						<li>
					<?php the_sub_field( 'list_item' ); ?> <i class="fa fa-check" aria-hidden="true"></i>
						</li>
				<?php endwhile; ?>
					</ul>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
				
			</div>
		</section>
			
			
		<?php elseif ( get_row_layout() == 'bonuses_section' ) : ?>
		<!-- BONUS SECTION -->	
		<section id="bonuses" class="bonus-section">
			<div class="container">
			<?php if ( have_rows( 'bonuses' ) ) : ?>
				<?php while ( have_rows( 'bonuses' ) ) : the_row(); ?>
				
				<div class="bonus-title"><?php the_sub_field( 'bonus_title' ); ?></div>
				<div class="bonus-text"><?php the_sub_field( 'bonus_text' ); ?>
					<?php if ( get_sub_field( 'testimonial' ) == 1 ) { ?>
					 <?php $testimonial_image = get_sub_field( 'testimonial_image' ); ?>
					<?php if ( $testimonial_image ) { ?>
					<hr>
					<div class="auth-img"><img src="<?php echo $testimonial_image['url']; ?>" alt="<?php echo $testimonial_image['alt']; ?>" /></div>
					<?php } ?>
					<div class="auth-text"><?php the_sub_field( 'testimonial_text' ); ?></div>
					<?php } else { 
					 echo ''; 
					} ?>
					
					
				</div>
				<?php endwhile; ?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
			</div>
		</section>
		
			
		<?php elseif ( get_row_layout() == 'mission_block' ) : ?>
		<!-- MISSION BLOCK -->
		<section class="mission-section">
			<div class="container mission-block">
				<div class="mission-text">
				<div class="mission-title"><?php the_sub_field( 'mission_title' ); ?></div>
				<?php the_sub_field( 'mission_text' ); ?>
				</div>
			</div>
		</section>
			
			
		<?php elseif ( get_row_layout() == 'numbered_items_section' ) : ?>
		<!-- NUMBERED SECTION -->
		<div id="number-block-graphic"></div>
		<section class="numbered-section">
			<div class="container">
				<h2><?php the_sub_field( 'numbered_section_intro_text' ); ?></h2>
			<?php if ( have_rows( 'numbered_item' ) ) :  $i = 1; ?>
				<?php while ( have_rows( 'numbered_item' ) ) : the_row(); ?>
					<div class="item">
				<span class="num-circle"><?php echo $i; $i++; ?></span><div class="item-text"><?php the_sub_field( 'item_text' ); ?></div>
					
					<?php if ( get_sub_field( 'item_list' ) == 1 ) { ?>
					<div class="list-box">
					 <?php the_sub_field( 'item_list_block' ); ?>
					</div>
					<?php } else { 
					 echo ''; 
					} ?>
					<?php if ( get_sub_field( 'item_photo_list' ) == 1 ) { ?>
					<div class="photo-list-box">
						<h4><?php the_sub_field( 'item_photo_list_intro' ); ?></h4>
					 <?php if ( have_rows( 'item_photo_block' ) ) : ?>
						<?php while ( have_rows( 'item_photo_block' ) ) : the_row(); ?>
							<?php $photo_list_image = get_sub_field( 'photo_list_image' ); ?>
							<?php if ( $photo_list_image ) { ?>
								<div class="photo-listing">
								<div class="listing-left"><img src="<?php echo $photo_list_image['url']; ?>" alt="<?php echo $photo_list_image['alt']; ?>" /></div>
							<?php } ?>
						<div class="listing-right"><?php the_sub_field('photo_list_text'); ?></div>
						</div>
							
						<?php endwhile; ?>
					<?php else : ?>
						<?php // no rows found ?>
					<?php endif; ?>
						<div class="photo-list-outro-text"><?php the_sub_field( 'item_photo_list_outro' ); ?></div>
					</div>
					<?php } else { 
					 echo ''; 
					} ?>
				</div><hr />
				<?php endwhile; ?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
			</div>
		</section>
		<div id="number-block-graphic-2"></div>
			
				
		<?php elseif ( get_row_layout() == 'testimonials_section' ) : ?>
		<!-- TESTIMONIAL SECTION -->
		<section class="testimonial-section">
			<div class="container">
			<?php if ( have_rows( 'testimonials' ) ) : ?>
				<?php while ( have_rows( 'testimonials' ) ) : the_row(); ?>
				<div class="testimonial">
					<?php $testimonial_image = get_sub_field( 'testimonial_image' ); ?>
					<?php if ( $testimonial_image ) { ?>
					<div class="auth-img"><img src="<?php echo $testimonial_image['url']; ?>" alt="<?php echo $testimonial_image['alt']; ?>" /></div>
					<?php } ?>
					<div class="auth-txt">
						<div class="auth-pull-quote"><?php the_sub_field( 'testimonial_intro_text' ); ?></div>
						<div class="auth-quote"><?php the_sub_field( 'testimonial_text' ); ?></div>
						<div class="auth"><?php the_sub_field('testimonial_author'); ?></div>
					</div>
				</div>
				<?php endwhile; ?>
			<?php else : ?>
				<?php // no rows found ?>
			<?php endif; ?>
			</div>
		</section>
		
		
		
		<?php endif; ?>
	<?php endwhile; ?>
<?php else: ?>
	<?php // no layouts found ?>
<?php endif; ?>
  

</article>
<?php get_footer(); ?>